package com.example.elazar.kotlindemo.data

import com.google.gson.annotations.SerializedName

class Movie(imageUrl : String, @SerializedName("title") val title: String) {

    @SerializedName("poster_path")
    val imageUrl : String = imageUrl
        get() = BASE_IMAGES_URL + field

    companion object {
        private const val BASE_IMAGES_URL = "https://image.tmdb.org/t/p/w440_and_h660_bestv2"
    }
}