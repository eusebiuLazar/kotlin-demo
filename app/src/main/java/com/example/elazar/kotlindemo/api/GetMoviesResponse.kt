package com.example.elazar.kotlindemo.api

import com.example.elazar.kotlindemo.data.Movie
import com.google.gson.annotations.SerializedName

data class GetMoviesResponse(@SerializedName("results") val movies: List<Movie>)