package com.example.elazar.kotlindemo.adapters.viewholders

import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.elazar.kotlindemo.R

class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var image: ImageView = ViewCompat.requireViewById(view, R.id.image)
    var title: TextView = ViewCompat.requireViewById(view, R.id.title)
}