package com.example.elazar.kotlindemo.api

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesApi {
    @GET("movie/popular")
    fun getPopular(@Query("api_key") apiKey : String, @Query("page") page : Int) : Single<GetMoviesResponse>

    companion object {
        private const val BASE_URL : String = "https://api.themoviedb.org/3/"
        private const val API_KEY = "6dfb4714bc5d5051888a51101b2571cd"

        private lateinit var moviesApi : MoviesApi

        private fun get() : MoviesApi {
            if (!this::moviesApi.isInitialized) {
                val retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()

                moviesApi = retrofit.create(MoviesApi::class.java)
            }

            return moviesApi
        }

        fun getPopular(page : Int) : Single<GetMoviesResponse> {
            return get().getPopular(API_KEY, page)
        }
    }
}