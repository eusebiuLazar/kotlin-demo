package com.example.elazar.kotlindemo.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.example.elazar.kotlindemo.api.MoviesApi
import com.example.elazar.kotlindemo.data.Movie
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel : ViewModel() {
    val moviesData: MutableLiveData<List<Movie>> = MutableLiveData()
    val disposables : CompositeDisposable = CompositeDisposable()

    fun getMovies(page : Int) {
        disposables.add(MoviesApi.getPopular(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({response -> moviesData.value = response.movies}, {e -> e.printStackTrace()}))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}