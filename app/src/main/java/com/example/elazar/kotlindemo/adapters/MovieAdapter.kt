package com.example.elazar.kotlindemo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.elazar.kotlindemo.R
import com.example.elazar.kotlindemo.adapters.viewholders.MovieViewHolder
import com.example.elazar.kotlindemo.data.Movie
import com.squareup.picasso.Picasso

class MovieAdapter : RecyclerView.Adapter<MovieViewHolder>() {
    lateinit var movies: List<Movie>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.v_movie_item, parent, false))
    }

    override fun getItemCount(): Int {
        return if (this::movies.isInitialized) {
            movies.size
        } else {
            0
        }
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie: Movie = movies[position]
        holder.title.text = movie.title
        Picasso.get()
                .load(movie.imageUrl)
                .into(holder.image)
    }
}