package com.example.elazar.kotlindemo.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import com.example.elazar.kotlindemo.R
import com.example.elazar.kotlindemo.adapters.MovieAdapter
import com.example.elazar.kotlindemo.data.Movie
import android.support.v7.widget.LinearLayoutManager
import com.example.elazar.kotlindemo.viewmodels.MainViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeLayout: SwipeRefreshLayout

    private var adapter: MovieAdapter = MovieAdapter()
    private lateinit var viewModel : MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.a_main)
        getViewRefs()
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.moviesData.observe(this, Observer { t -> onMoviesLoaded(t) })

        recyclerView.adapter = adapter
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        swipeLayout.setOnRefreshListener({
            getMovies()
        })

        getMovies()
    }

    private fun getViewRefs() {
        swipeLayout = ActivityCompat.requireViewById(this, R.id.swipe_layout)
        recyclerView = ActivityCompat.requireViewById(this, R.id.recycler)
    }

    private fun getMovies() {
        if (!swipeLayout.isRefreshing) {
            swipeLayout.isRefreshing = true
        }

        viewModel.getMovies(1)
    }

    fun onMoviesLoaded(movies : List<Movie>?) {
        if (swipeLayout.isRefreshing) {
            swipeLayout.isRefreshing = false
        }

        movies?.let {
            adapter.movies = movies
            adapter.notifyDataSetChanged()
        }
    }
}
